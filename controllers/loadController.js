const { User, Truck, Load } = require("../model/model");
const jwt = require("jsonwebtoken");
const ObjectId = require("mongodb").ObjectId;

const truckType = [
  [300, 250, 170, 1700],
  [500, 250, 170, 2500],
  [700, 350, 200, 4000],
];

const loadController = {
  // GET user's load
  getLoad: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;

    const filter = req.query.status;
    const offset = parseInt(req.query.offset);
    const limit = parseInt(req.query.limit) + offset;

    try {
      const user = await User.findById(userId);
      const loads = await Load.find({ created_by: userId });
      const result = loads.filter((n) => n.status === filter);
      if (user.role === "SHIPPER") {
        res.status(200).json({
          loads: result.splice(offset, limit),
        });
      } else res.status(400).json({ message: "user is not Shipper" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //ADD load
  addLoad: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    const created_date = new Date().toISOString();
    try {
      const user = await User.findById(userId);
      if (user.role === "SHIPPER") {
        const newLoad = new Load({
          ...req.body,
          created_by: userId,
          created_date: created_date,
        });

        await newLoad.save();
        res.status(200).json({ message: "Load created successfully" });
      } else res.status(400).json({ message: "User's role is not Shipper" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //GET active Load by id
  getActiveLoadId: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const loads = await Load.find();
      const user = await User.findById(userId);
      if (user.role === "DRIVER") {
        const result = loads.filter((n) => n.status !== "SHIPPED");
        res.status(200).json({
          loads: result,
        });
      } else res.status(400).json({ message: "User is not DRIVER" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //Iterate to next Load state
  nextState: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const id = new ObjectId(userId);

      const load = await Load.findOne({ assigned_to: id });
      const user = await User.findById(userId);
      const state = [
        "En route to Pick Up",
        "Arrived to Pick Up",
        "En route to delivery",
        "Arrived to delivery",
      ];
      const stateIndex = parseInt(state.indexOf(load.state));

      if (user.role === "DRIVER") {
        if (load.status !== "SHIPPED") {
          if (stateIndex < 2) {
            await Load.findByIdAndUpdate(load._id, {
              state: state[stateIndex + 1],
            });
            res.status(200).json({
              message: `Load state changed to '${state[stateIndex + 1]}'`,
            });
          } else if (stateIndex === 2) {
            await Load.findByIdAndUpdate(load._id, {
              state: "Arrived to delivery",
              status: "SHIPPED",
            });
            await Truck.updateOne(
              { assigned_to: userId },
              { $set: { status: "IS" } }
            );

            res.status(200).json({
              message: `Load state changed to '${state[stateIndex + 1]}'`,
            });
          }
        } else {
          res.status(400).json({ message: "Load is already SHIPPED" });
        }
      }
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //GET Load by id
  getLoadId: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);

      const load = await Load.findById(req.params.id);
      if (user.role === "SHIPPER") {
        res.status(200).json(load);
      } else res.status(400).json({ message: "User is not a shipper" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //UPDATE user's load
  updateLoad: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);

      if (user.role === "SHIPPER") {
        await Load.findByIdAndUpdate(req.params.id, {
          $set: {
            name: req.body.name,
            payload: req.body.payload,
            pickup_address: req.body.pickup_address,
            delivery_address: req.body.delivery_address,
            dimensions: req.body.dimensions,
          },
        });

        res.status(200).json({ message: "Load details changed successfully" });
      } else res.status(400).json({ message: "User is not a shipper" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //DELETE user's load by id
  deleteLoad: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);
      if (user.role === "SHIPPER") {
        await Load.deleteOne({ _id: req.params.id });

        res.status(200).json({ message: "Load deleted successfully" });
      }
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //POST a user's load by id
  postLoad: async (req, res) => {
    const accessToken = req.headers.token;
    const created_date = new Date().toISOString();
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);
      if (user.role === "SHIPPER") {
        const truck = await Truck.find({ assigned_to: { $ne: null } });
        const load = await Load.findById(req.params.id, {});

        const isTruck = truck.filter((n) => n.status === "IS");
        if (load.status === "NEW" || load.status === "POSTED") {
          await Load.findByIdAndUpdate(req.params.id, { status: "POSTED" });
          const loadType = [];
          let result = [];
          if (
            load.dimensions.width <= truckType[0][0] &&
            load.dimensions.length <= truckType[0][1] &&
            load.dimensions.height <= truckType[0][2] &&
            load.payload <= truckType[0][3]
          ) {
            loadType.push("SPRINTER");
          }
          if (
            load.dimensions.width <= truckType[1][0] &&
            load.dimensions.length <= truckType[1][1] &&
            load.dimensions.height <= truckType[1][2] &&
            load.payload <= truckType[1][3]
          ) {
            loadType.push("SMALL STRAIGHT");
          }
          if (
            load.dimensions.width <= truckType[2][0] &&
            load.dimensions.length <= truckType[2][1] &&
            load.dimensions.height <= truckType[2][2] &&
            load.payload <= truckType[2][3]
          ) {
            loadType.push("LARGE STRAIGHT");
          }

          if (isTruck.length > 0) {
            for (n of isTruck) {
              if (loadType.includes(n.type)) {
                result = n;
                break;
              }
            }

            if (Object.keys(result).length > 0) {
              await Load.updateOne(
                { _id: req.params.id },
                {
                  $set: {
                    status: "ASSIGNED",
                    state: "En route to Pick Up",
                    assigned_to: assigned_to,
                    logs: {
                      message: `Load assigned to driver with id ${assigned_to}`,
                      time: created_date,
                    },
                  },
                }
              );
              await Truck.findByIdAndUpdate(result._id, {
                $set: { status: "OL" },
              });
              res.status(200).json({
                message: "Load posted successfully",
                driver_found: true,
              });
            } else
              res.status(200).json({
                message: "Load posted successfully",
                driver_found: false,
              });
          } else
            res.status(200).json({
              message: "Load already posted",
            });
        } else res.status(400).json({ message: "Load is already post" });
      }
    } catch (err) {
      res.status(500).json({ message: "error" });
    }
  },

  //Get Load shipping info

  loadInfo: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;

    try {
      const id = new ObjectId(userId);
      const user = await User.findById(userId);
      const truck = await Truck.findOne({ assigned_to: id });
      const load = await Load.findById(req.params.id);
      if (user.role === "SHIPPER") {
        res.status(200).json({ load, truck });
      } else res.status(400).json({ message: "User is not Shipper" });
    } catch (err) {
      res.status(500).json({ message: "error" });
    }
  },
};

module.exports = loadController;
