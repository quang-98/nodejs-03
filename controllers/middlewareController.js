const jwt = require("jsonwebtoken");

const middlewareController = {
  //verifyToken
  verifyToken: async (req, res, next) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];

    if (token) {
      jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
        if (err) {
          res.status(400).json({ message: "token not valid" });
        }
        req.user = user;
        next();
      });
    } else {
      res.status(400).json({ message: "not authenticated" });
    }
  },
};

module.exports = middlewareController;
