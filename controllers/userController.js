const { User, Truck, Load } = require("../model/model");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const userController = {
  //GET User
  getUser: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;

    try {
      const users = await User.findById(userId);

      res.status(200).json({
        user: {
          id: users._id,
          role: users.role,
          email: users.email,
          created_date: users.created_date,
        },
      });
    } catch (err) {
      res.status(500).send({ message: "string" });
    }
  },

  //DELETE User
  deleteUser: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      await User.deleteOne({ _id: userId });
      res.status(200).send({ message: "Profile deleted successfully" });
    } catch (err) {
      res.status(500).send({ message: "error" });
    }
  },

  //Change password
  changePassword: async (req, res) => {
    const { oldPassword, newPassword: plainTextPassword } = req.body;

    if (!plainTextPassword || typeof plainTextPassword !== "string") {
      return res.status(400).json({ message: "Invalid password" });
    }

    if (plainTextPassword.length < 6) {
      return res.status(400).json({
        message: "Password too small. Should be at least 6 characters",
      });
    }
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const _id = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(_id);

      if (await bcrypt.compare(oldPassword, user.password)) {
        const password = await bcrypt.hash(plainTextPassword, 10);

        await User.updateOne(
          { _id },
          {
            $set: { password },
          }
        );

        res.status(200).json({ message: "Password changed successfully" });
      } else res.status(400).json({ message: "wrong password" });
    } catch (err) {
      res.status(500).json({ message: "error" });
    }
  },
};
module.exports = userController;
