const { Truck, Load, User } = require("../model/model.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const authController = {
  // create newUser
  createUser: async (req, res) => {
    const { email, password: plainTextPassword } = req.body;

    if (!email || typeof email !== "string") {
      return res.status(400).json({ message: "Invalid password" });
    }

    if (!plainTextPassword || typeof plainTextPassword !== "string") {
      return res.status(400).json({ message: "Invalid password" });
    }

    if (plainTextPassword.length < 6) {
      return res.status(400).json({
        message: "Password too small. Should be atleast 6 characters",
      });
    }
    const password = await bcrypt.hash(plainTextPassword, 10);

    try {
      const created_date = new Date().toISOString();
      const newUser = new User({
        email: email,
        password: password,
        role: req.body.role,
        created_date: created_date,
      });
      await newUser.save();
      res.status(200).json({ message: "Success" });
    } catch (err) {
      if (err.code === 11000) {
        return res.status(400).json({ message: "User is already used" });
      }
      res.status(500).json({ message: "error" });
    }

    res.status(200).json({ message: "Success" });
  },

  //Login User

  loginUser: async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email }).lean();

      if (!user) {
        return res.status(400).json({ message: "Invalid email/password" });
      }

      if (await bcrypt.compare(password, user.password)) {
        const token = jwt.sign(
          { id: user._id, email: user.email },
          process.env.JWT_SECRET
        );

        return res.status(200).json({ message: "Success", jwt_token: token });
      }
    } catch (error) {
      res.status(400).json({ message: "error" });
    }
  },

  //Forgot User

  forgotPassword: async (req, res) => {
    try {
      const user = await User.findOne({ email: req.body.email });

      if (Object.keys(user).length) {
        res
          .status(200)
          .json({ message: "New password sent to your email address" });
      } else res.status(400).json({ message: "email is not exist" });
    } catch (err) {
      res.status(400).json({ message: "error" });
    }
  },
};

module.exports = authController;
