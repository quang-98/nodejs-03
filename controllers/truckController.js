const { User, Truck, Load } = require("../model/model");
const jwt = require("jsonwebtoken");

const truckController = {
  // GET user's truck
  getTruck: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);

      if (user.role === "DRIVER") {
        const trucks = await Truck.find({ created_by: userId });
        res.status(200).json({
          trucks,
        });
      } else res.status(400).json({ message: "User's role is not Driver" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //ADD truck
  addTruck: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    const created_date = new Date().toISOString();
    try {
      const user = await User.findById(userId);

      if (user.role === "DRIVER") {
        const newTruck = new Truck({
          ...req.body,
          created_by: userId,
          created_date: created_date,
        });

        await newTruck.save();
        res.status(200).json({ message: "Truck created successfully" });
      } else
        res
          .status(400)
          .json({ message: "User's role is not Driver", err: user.role });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //GET Truck by id
  getTruckId: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);
      const trucks = await Truck.findById(req.params.id);
      if (user.role === "DRIVER") {
        res.status(200).json(trucks);
      } else res.status(400).json({ message: "User's role is not Driver" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //UPDATE user's truck
  updateTruck: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);
      const type = req.body;
      if (user.role === "DRIVER") {
        await Truck.findByIdAndUpdate(req.params.id, type);
        res.status(200).json({ message: "Truck details changed successfully" });
      } else res.status(400).json({ message: "User's role is not Driver" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //DELETE user's truck by id
  deleteTruck: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);
      await Truck.deleteOne({ _id: req.params.id });
      if (user.role === "DRIVER") {
        res.status(200).json({ message: "Truck deleted successfully" });
      } else res.status(400).json({ message: "User's role is not Driver" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },

  //ASSIGN truck to user by ID
  assignTruck: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(userId);

      if (user.role === "DRIVER") {
        truck = await Truck.find({ assigned_to: userId });

        if (truck[0].status === "IS") {
          await Truck.updateOne(
            { assigned_to: userId },
            { $unset: { assigned_to: "" } }
          );
          await Truck.updateOne(
            { _id: req.params.id },
            { $set: { assigned_to: userId } }
          );
          res.status(200).json({ message: "Truck assigned successfully" });
        }
        res.status(400).json({ message: "Truck is on load" });
      } else res.status(400).json({ message: "User's role is not Driver" });
    } catch (err) {
      res.status(500).json({ message: "string" });
    }
  },
};

module.exports = truckController;
