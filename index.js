const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
var bodyParser = require("body-parser");
const morgan = require("morgan");
const dotenv = require("dotenv");

const userRoute = require("./routes/user");
const truckRoute = require("./routes/truck");
const loadRoute = require("./routes/load");
const authRoute = require("./routes/auth");

dotenv.config();
//CONNECT DATABASE
mongoose.connect(process.env.MONGODB_URL, () =>
  console.log("Connected to MongoDB")
);

app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());
app.use(morgan("common"));

app.get("/api", (req, res) => {
  res.status(200).json("hello");
});

//User route
app.use("/api/users/me", userRoute);

//Truck route
app.use("/api/trucks", truckRoute);

//Load route
app.use("/api/loads", loadRoute);

//Auth route
app.use("/api/auth", authRoute);

app.listen(8080, () => {
  console.log("server is running...");
});
