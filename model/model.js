const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      sparse: true,
    },
    role: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    created_date: {
      type: String,
      required: true,
    },
  },
  { collection: "users" }
);

const truckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
    sparse: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  type: {
    type: String,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
  },
  status: {
    type: String,
    enum: ["OL", "IS"],
    default: "IS",
  },
  created_date: {
    type: String,
    required: true,
  },
});

const loadSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  status: {
    type: String,
    enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
    default: "NEW",
  },
  state: {
    type: String,
    enum: [
      "En route to Pick Up",
      "Arrived to Pick Up",
      "En route to delivery",
      "Arrived to delivery",
    ],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  logs: {
    message: {
      type: String,
    },
    time: {
      type: String,
    },
  },
  created_date: {
    type: String,
    required: true,
  },
});

const User = mongoose.model("User", userSchema);
const Truck = mongoose.model("Truck", truckSchema);
const Load = mongoose.model("Load", loadSchema);

module.exports = { User, Truck, Load };
