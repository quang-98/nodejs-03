const loadController = require("../controllers/loadController");
const middlewareController = require("../controllers/middlewareController");

const router = require("express").Router();

//GET Load
router.get("/", middlewareController.verifyToken, loadController.getLoad);

//ADD Load
router.post("/", middlewareController.verifyToken, loadController.addLoad);

//GET user's active load
router.get(
  "/active",
  middlewareController.verifyToken,
  loadController.getActiveLoadId
);

//PATCH iterate to next Load state
router.patch(
  "/active/state",
  middlewareController.verifyToken,
  loadController.nextState
);

//GET Load by id
router.get("/:id", middlewareController.verifyToken, loadController.getLoadId);

//UPDATE user's Load by id
router.put("/:id", middlewareController.verifyToken, loadController.updateLoad);

//DELETE use's Load by id
router.delete(
  "/:id",
  middlewareController.verifyToken,
  loadController.deleteLoad
);

//POST a user's Load by id
router.post(
  "/:id/post",
  middlewareController.verifyToken,
  loadController.postLoad
);

//GET user's Load shipping info by id
router.get(
  "/:id/shipping_info",
  middlewareController.verifyToken,
  loadController.loadInfo
);

module.exports = router;
