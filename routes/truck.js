const truckController = require("../controllers/truckController");
const middlewareController = require("../controllers/middlewareController");

const router = require("express").Router();

//GET truck
router.get("/", middlewareController.verifyToken, truckController.getTruck);

//ADD truck
router.post("/", middlewareController.verifyToken, truckController.addTruck);

//GET truck by id
router.get(
  "/:id",
  middlewareController.verifyToken,
  truckController.getTruckId
);

//UPDATE user's truck by id
router.put(
  "/:id",
  middlewareController.verifyToken,
  truckController.updateTruck
);

//DELETE use's truck by id
router.delete(
  "/:id",
  middlewareController.verifyToken,
  truckController.deleteTruck
);

//ASSIGN truck to user by id
router.post(
  "/:id/assign",
  middlewareController.verifyToken,
  truckController.assignTruck
);

module.exports = router;
