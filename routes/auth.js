const authController = require("../controllers/authController");

const router = require("express").Router();

//REGISTER user
router.post("/register", authController.createUser);

//LOGIN user
router.post("/login", authController.loginUser);

//FORGOT password
router.post("/forgot_password", authController.forgotPassword);

module.exports = router;
