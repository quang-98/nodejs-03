const userController = require("../controllers/userController");
const middlewareController = require("../controllers/middlewareController");

const router = require("express").Router();

//get user
router.get("/", middlewareController.verifyToken, userController.getUser);

//delete user
router.delete("/", middlewareController.verifyToken, userController.deleteUser);

//patch user's password
router.patch(
  "/password",
  middlewareController.verifyToken,
  userController.changePassword
);

module.exports = router;
